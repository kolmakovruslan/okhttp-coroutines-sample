package io.kolmakovruslan.okhttpcoroutinessample

import kotlinx.coroutines.experimental.CoroutineDispatcher
import kotlinx.coroutines.experimental.Runnable
import java.util.concurrent.Executors
import kotlin.coroutines.experimental.CoroutineContext

object BG : CoroutineDispatcher() {

    private val executor = Executors.newSingleThreadExecutor()

    override fun dispatch(context: CoroutineContext, block: Runnable) {
        executor.submit(block)
    }
}