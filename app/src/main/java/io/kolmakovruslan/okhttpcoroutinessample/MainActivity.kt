package io.kolmakovruslan.okhttpcoroutinessample

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Button
import android.widget.LinearLayout
import android.widget.LinearLayout.VERTICAL
import io.kolmakovruslan.okhttpcoroutinessample.simpleAsyncLoading.SimpleAsyncLoadingActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val contentLayout = LinearLayout(this).apply {
            orientation = VERTICAL
        }
        contentLayout.run {
            val asyncSampleBtn = Button(context).apply {
                setText(R.string.simple_async_loading_activity_title)

                setOnClickListener {
                    val intent = Intent(context, SimpleAsyncLoadingActivity::class.java)
                    startActivity(intent)
                }
            }
            addView(asyncSampleBtn)
        }
        setContentView(contentLayout)
    }
}