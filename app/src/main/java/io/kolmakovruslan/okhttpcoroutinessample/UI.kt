package io.kolmakovruslan.okhttpcoroutinessample

import android.os.Handler
import android.os.Looper
import kotlinx.coroutines.experimental.CoroutineDispatcher
import kotlinx.coroutines.experimental.Runnable
import kotlin.coroutines.experimental.CoroutineContext

object UI : CoroutineDispatcher() {

    private val mainHandler = Handler(Looper.getMainLooper())

    override fun dispatch(context: CoroutineContext, block: Runnable) {
        mainHandler.post(block)
    }
}