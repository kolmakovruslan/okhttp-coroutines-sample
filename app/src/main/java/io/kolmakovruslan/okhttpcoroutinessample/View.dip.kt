package io.kolmakovruslan.okhttpcoroutinessample

import android.view.View

fun View.dip(value: Int): Int = (value * resources.displayMetrics.density).toInt()