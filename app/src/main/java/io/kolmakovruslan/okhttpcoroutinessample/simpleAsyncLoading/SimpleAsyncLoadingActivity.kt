package io.kolmakovruslan.okhttpcoroutinessample.simpleAsyncLoading

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import io.kolmakovruslan.okhttpcoroutinessample.BG
import io.kolmakovruslan.okhttpcoroutinessample.UI
import io.kolmakovruslan.okhttpcoroutinessample.dip
import kotlinx.coroutines.experimental.launch
import okhttp3.OkHttpClient

class SimpleAsyncLoadingActivity : AppCompatActivity() {

    private lateinit var logView: TextView

    private val okHttpClient = OkHttpClient()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Создадим простую вёрстку для вывода информации о выполнении задачи
        logView = TextView(this).apply {
            textSize = 17f
            setPadding(dip(16), dip(16), dip(16), dip(16))
        }
        setContentView(logView)
    }

    override fun onResume() {
        super.onResume()

        pushToLog("Сообщение до начала загрузки")

        // Звпустим новую задачу в основном потоке
        launch(UI) {
            // Создадим задачу для загрузки текста из интернета,
            // для выполнения в фоне
            val job = GetTextFromWebJobBuilder()
                .build(
                    httpClient = okHttpClient,
                    context = BG,
                    url = "https://raw.githubusercontent.com/Kotlin/kotlin-coroutines/master/LICENSE"
                )

            // Дождёмся завершения загрузки
            val resultText = job.await()

            // Выведем в лог информацию о завршении и полученный текст
            pushToLog("Загрузка завершена")
            pushToLog(resultText)
        }

        pushToLog("Сообщение после начала загрузки")
    }

    private fun pushToLog(text: String) {
        logView.text = "${logView.text}\n$text"
    }
}