package io.kolmakovruslan.okhttpcoroutinessample.simpleAsyncLoading

import kotlinx.coroutines.experimental.async
import okhttp3.OkHttpClient
import okhttp3.Request
import kotlin.coroutines.experimental.CoroutineContext

class GetTextFromWebJobBuilder {

    fun build(
        httpClient: OkHttpClient,
        context: CoroutineContext,
        url: String
    ) = async(context) {
        val request = Request.Builder()
            .url(url)
            .build()
        val response = httpClient.newCall(request).execute()
        val responseText = response.body()!!.string()
        responseText
    }
}